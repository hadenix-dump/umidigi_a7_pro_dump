#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:a092637a94d38bd06fa973d3414f8dfb3275f4a6; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/bootdevice/by-name/boot:33554432:c807a581f53de89d4bc627c7cc118c9b402d5bbc \
          --target EMMC:/dev/block/platform/bootdevice/by-name/recovery:33554432:a092637a94d38bd06fa973d3414f8dfb3275f4a6 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
